
import pickle
import numpy as np
import cosmoHammer.Constants as c

class ExtSampleFileUtil(object):
	"""
	Util for handling sample files
	
	:param filePrefix: the prefix to use
	:param master: True if the sampler instance is the master
	:param reuseBurnin: True if the burn in data from a previous run should be used
	:param lognames: list of ctx keys you want to log during sampling
	
	"""
	
	def __init__(self, filePrefix, lognames=[], master=True, reuseBurnin=False):
		self.filePrefix = filePrefix
		
		if(master):
			if(reuseBurnin):
				mode = "r"
			else:
				mode = "w"
			self.samplesFileBurnin = open(self.filePrefix+c.BURNIN_SUFFIX, mode)
			self.probFileBurnin = open(self.filePrefix+c.BURNIN_PROB_SUFFIX, mode)
			
			self.samplesFile = open(self.filePrefix+c.FILE_SUFFIX, "w")
			self.probFile = open(self.filePrefix+c.PROB_SUFFIX, "w")


			self.lognames = lognames

			self.loggingFileBurnin = open(self.filePrefix + "burnin_logging.out", mode)
			for i in lognames:
				self.loggingFileBurnin.write(i + '\t')
			self.loggingFileBurnin.write("\n")

			self.loggingFile = open(self.filePrefix + "_logging.out", "w")
			for i in lognames:
				self.loggingFile.write(i + '\t')
			self.loggingFile.write("\n")
	
	def importFromFile(self, filePath):
		values = np.loadtxt(filePath, dtype=float)
		return values

	def storeRandomState(self, filePath, randomState):
		with open(filePath,'wb') as f:
			pickle.dump(randomState, f)

	def importRandomState(self, filePath):
		with open(filePath,'rb') as f:
			state = pickle.load(f)
		return state

	def persistBurninValues(self, pos, prob, data):
		self.persistValues(self.samplesFileBurnin, self.probFileBurnin, self.loggingFileBurnin, pos, prob, data)
		
	def persistSamplingValues(self, pos, prob, data):
		self.persistValues(self.samplesFile, self.probFile, self.loggingFile, pos, prob, data)
		

	def persistValues(self, posFile, probFile, logFile, pos, prob, data):
		"""
		Writes the walker positions and the likelihood to the disk
		"""
		posFile.write("\n".join(["\t".join([str(q) for q in p]) for p in pos]))
		posFile.write("\n")
		posFile.flush()
		
		probFile.write("\n".join([str(p) for p in prob]))
		probFile.write("\n")
		probFile.flush()

		for d in data:
			for name in self.lognames:
				logFile.write(str(d[name])+"\t")
			logFile.write("\n")
			logFile.flush()
		
	def close(self):
		self.samplesFileBurnin.close()
		self.probFileBurnin.close()
		self.samplesFile.close()
		self.probFile.close()
		self.loggingFile.close()
		self.loggingFileBurnin.close()

	def __str__(self, *args, **kwargs):
		return "SampleFileUtil"