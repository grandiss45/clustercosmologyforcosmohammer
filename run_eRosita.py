__author__ = 'sebastiangrandis'


import numpy as np
from cosmoHammer import LikelihoodComputationChain, CosmoHammerSampler, MpiCosmoHammerSampler
from ClusterCosmologyModules import *
from ExtSampleFileUtil import ExtSampleFileUtil
from RestartFromChain import RestartFromChain

# define Constants for theory prediction, naming as in Class
DEFAULT_PARAMS = {'N_ncdm': 0,
                  'Omega_k': 0.0,
                  'gamma': 0.55,
                  'w0_fld': -1.,
                  'wa_fld': 0.,
                  'n_s': 0.968,
                  'DE_perturb': True
                   }

# define mapping of the sampling parameters
COSMOLOGY_PARAMETER_MAPPING = {'h': 0,
                               'Omega_b': 1,
                               'Omega_cdm': 2,
                               'ln10^{10}A_s': 3,
                               }

CLUSTER_PARAMETER_MAPPING = {'A_LM': 4,
                             'B_LM': 5,
                             'C_LM': 6,
                             'D_LM': 7
                             }

# define the sampling parameters. params[:,1] is the lower bound of the box, params[:,2] the upper bound,
# params[:,0] the mean and params[:,4] the standard deviation of the initial position generator.
params = np.array([[0.738, 0.6, 0.85, 0.024],
                   [0.0404, 0.02, 0.06, 0.001],
                   [0.25, 0.1, 0.5, 0.05],
                   [3., 2., 4., 0.1],
                   [1.61, 0., 3., 0.14],
                   [101.483, 100., 103., 0.085],
                   [1.85, 0., 4., 0.42],
                   [0.396, 0.2, 0.6, 0.039]])

# define the likelihood computation chain using the choosen bounds
chain = LikelihoodComputationChain(params[:, 1], params[:, 2])

# create an instance of our theory prediction module...
massfct = MassfunctionCoreModule_PyCamb(constants=DEFAULT_PARAMS,
                                 cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING,
                                 astro_mapping=CLUSTER_PARAMETER_MAPPING)
# and add it to the chain
chain.addCoreModule(massfct)

# create instance of the priors and likelihoods you want to sample...
number_count = XRayLuminosityLikelihoodModule(astro_mapping=CLUSTER_PARAMETER_MAPPING)
bbns = BigBangNucleoSynthesisPrior(cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING)
hubble = HubbleConstantPrior(cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING)
LM = MassLuminisityPrior(mapping=CLUSTER_PARAMETER_MAPPING)
# and add them to the chain
chain.addLikelihoodModule(bbns)
chain.addLikelihoodModule(hubble)
chain.addLikelihoodModule(number_count)
chain.addLikelihoodModule(LM)

# setup the chain
chain.setup()

# setting up the storage Util
fileprefix = 'erosita_test'  # prefix of all output files, can also be path
lognames = ['sigma8', 'Omega_m']  # key argument of the context.getData() entries, you want to output
storage = ExtSampleFileUtil(fileprefix, lognames=lognames)
# ini_pos = RestartFromChain('erosita_test2.out')

# initializing the sampler, choose MpiCosmoHammerSampler(same input) for parallel computation
sampler = MpiCosmoHammerSampler(params=params,
                                likelihoodComputationChain=chain,
                                filePrefix=fileprefix,
                                walkersRatio=12,
                                burninIterations=0,
                                sampleIterations=1000,
                                storageUtil=storage) #,
                                # initPositionGenerator=ini_pos)

# start sampling
sampler.startSampling()
