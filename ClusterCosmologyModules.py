__author__ = 'sebastiangrandis'

from classy import Class
import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import quad
from scipy.ndimage import gaussian_filter
import pycamb as pc
from hope import jit

DEFAULT_PARAMS = {'N_ncdm': 0,
                  'Omega_k': 0.0,
                  'gamma': 0.55,
                  'w0_fld': -1.,
                  'wa_fld': 0.,
                  'DE_perturb': True
                  }

CLASS_OUTPUT_PARAMS = {'output': 'mPk',
                       'P_k_max_h/Mpc': 15.,
                       'z_max_pk': 2.
                       }

COSMOLOGY_PARAMETER_MAPPING = {'h': 0,
                               'Omega_b': 1,
                               'Omega_cdm': 2,
                               'n_s': 3,
                               'ln10^{10}A_s': 4
                               }

CLUSTER_PARAMETER_MAPPING = {'A_LM': 5,
                             'B_LM': 6,
                             'C_LM': 7,
                             'D_LM': 8
                             }

def trapz(y, x):
    return 0.5*np.sum( (x[1:]-x[:-1])*(y[1:]+y[:-1]), axis=0 )

class MassfunctionCoreModule(object):

    def __init__(self, constants=DEFAULT_PARAMS,
                 output=CLASS_OUTPUT_PARAMS,
                 cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING,
                 astro_mapping=CLUSTER_PARAMETER_MAPPING,
                 selection_observable='LX',
                 sel_obs_min=42, sel_obs_max=47, sel_obs_bin=120,
                 z_min=0., z_max=2., z_bin=200,
                 k_min=-5., k_max=1., k_bin=500,
                 f_sky=0.68):

        self.cosmo_mapping = cosmo_mapping
        self.astro_mapping = astro_mapping
        self.constants = constants
        self.output = output

        # flag determining the selection variable, only implemented LX selection
        self.selection_observable = selection_observable

        # setting up the grid in selection observable and redshift
        self.len_sel_obs = sel_obs_bin  # mass and selection observable bin have same size
        self.len_z = z_bin  # number of redshift bins
        self.len_k = k_bin
        # sel.obs. array, will be converted to mass
        self.sel_obs_arr = np.logspace(sel_obs_min, sel_obs_max, sel_obs_bin)
        self.z_arr = np.linspace(z_min, z_max, z_bin)  # redshift array
        self.k_arr = np.logspace(k_min, k_max, k_bin)  # wavenumber array

        # modified gravity needs special treatment
        self.gamma = 0.55

        # fraction of skz covered by the survey
        self.f_sky = f_sky

        # initialize cosmology only once
        self.cosmology = Class()
        self.state = False

        return

    def setup(self):

        return

    def __call__(self, ctx):

        p1 = ctx.getParams()

        cosmo_params = self.constants.copy()  # saving the constants
        cosmo_params.update(self.output)  # appending output parameters

        for key, value in self.cosmo_mapping.items():
            cosmo_params[key] = p1[value]  # adding the relevant cosmological parameters

        # popping gamma and setting for modified growth
        try:
            self.gamma = cosmo_params.pop('gamma')
            if self.gamma != 0.55:
                cosmo_params['z_max_pk'] = 10.
        except KeyError:
            pass

        # set up correctly the dark energy density
        de_perturb = cosmo_params.pop('DE_perturb')
        try:
            if cosmo_params['w0_fld'] == -1. and cosmo_params['wa_fld']==0:
                cosmo_params.pop('w0_fld')
                cosmo_params.pop('wa_fld')
            else:
                if de_perturb:  # include neutrino density here
                    cosmo_params['Omega_fld'] = 1.-cosmo_params.pop('Omega_k')-cosmo_params['Omega_b']-cosmo_params['Omega_cdm']
                else:
                    cosmo_params['Omega_Lambda'] = 1.-cosmo_params.pop('Omega_k')-cosmo_params['Omega_b']-cosmo_params['Omega_cdm']
        except KeyError:
            pass

        astro_params = {}
        for key, value in self.astro_mapping.items():
            astro_params[key] = p1[value]  # saving the relevant astrophysical parameters

        if self.state:
            self.cosmology.struct_cleanup()

        # initialize a cosmology
        self.cosmology.set(cosmo_params)
        self.cosmology.compute()
        self.state = True

        # getting the mass array (in units of h M_solar) depending on cosmology and astrophysics
        M_arr = self.get_mass_array(astro_params)

        # computing the Matter Powerspectrum
        mPk, sigma8 = self.get_MatterPowerspectum_Class()

        # adding current sigma8 and Omega_m to the data, can be logged using ExtSampleFileUtil.py as storageFile
        ctx.getData()['sigma8'] = sigma8
        ctx.getData()['Omega_m'] = self.cosmology.Omega_m()

        # compute the number density of clusters on the grid defined by self.z_arr and M_arr
        # following Tinker et al. 2008
        dndM = self.calcMF(M_arr, mPk)

        # compute the redshift volume
        dV_z = self.zVol()

        # getting the massfunction
        dNdM = dndM*dV_z

        # normalizing for the covered sky fraction
        dNdM *= 4*np.pi*self.f_sky

        # average the mass function to the center of the bin
        dNdM_average = 0.25*(dNdM[1:, :-1]+dNdM[1:, 1:]+dNdM[:-1, :-1]+dNdM[:-1, 1:])

        ctx.add('Massfunction', dNdM_average)
        ctx.add('M_arr', M_arr)

        return

    def E_z(self, z):

        c = 2997.92458  # speed of light in 100 km/s

        if isinstance(z, float):
            # reduced hubble constant in dependence of redshift
            h_z = c*self.cosmology.Hubble(z)

            return h_z/self.cosmology.pars['h']
        else:
            # reduced hubble constant in dependence of redshift
            h_z = np.array([c*self.cosmology.Hubble(z[i]) for i in xrange(len(z))])

            return h_z/self.cosmology.pars['h']

    def zVol(self):

        dz = self.z_arr[2] - self.z_arr[1]
        c = 2997.92458  # speed of light in 100 km/s, Hubble distance in Mpc/h

        # Angular diameter distance in units of Mpc/h
        AngDiamDist_arr = np.array([self.cosmology.angular_distance(self.z_arr[i]) for i in xrange(self.len_z)])
        AngDiamDist_arr *= self.cosmology.pars['h']

        # comoving Volume per redshift bin
        deltaV = c*((1.+self.z_arr)*AngDiamDist_arr)**2 / self.E_z(self.z_arr)

        # ensuring that dV(z=0) != 0
        deltaV[0] = deltaV[1]

        return dz*deltaV[:, None]

    def get_mass_array(self, astro_params):
        # sets up the mass grid in units of M_solar/h following the respective scaling relations

        if self.selection_observable == 'LX':

            lnLX = np.log(self.sel_obs_arr)
            ALM = astro_params['A_LM']
            BLM = astro_params['B_LM']
            CLM = astro_params['C_LM']
            DLM = astro_params['D_LM']

            h = self.cosmology.pars['h']
            Ez = self.E_z(self.z_arr)

            # apply the scaling relation from Vikhlinin et al. 2009
            lnM = np.array([1./ALM*lnLX - CLM/ALM*np.log(Ez[i]) for i in xrange(self.len_z)])
            lnM -= BLM/ALM + 1.5/ALM*(DLM**2-0.396**2)
            lnM += 0.39/ALM*np.log(h/0.72)+np.log(3.9*10**14)

            # converting to masses in M_solar/h
            return np.exp(lnM)*self.cosmology.pars['h']

    def get_MatterPowerspectum_Class(self):

        # Careful, Class wants k, not k/h
        # k_in = np.empty((self.len_k,self.len_z,1))
        # for j in range(self.len_k):
        # k_in[j,:,0] = self.k_arr[j]*cosmology.pars['h']

        Pk = np.empty((self.len_k, self.len_z))

        # For standard growth
        if self.gamma == .55:
            for i in xrange(self.len_z):  # here no neutrinos
                Pk[:, i] = np.array([self.cosmology.pk(self.k_arr[j]*self.cosmology.pars['h'], self.z_arr[i]) for j in xrange(self.len_k)])
            # Pk = cosmology.get_pk(k_in,self.z_arr,self.len_k,self.len_z,1)[:,:,0]*cosmology.pars['h']**3
            Pk *= self.cosmology.pars['h']**3

        # For growth from gamma parametrization
        else:
            # Get PS at redshift z_ini
            z_ini = self.cosmology.pars['z_max_pk']
            a_ini = 1./(1.+z_ini)
            PK_10 = np.array([self.cosmology.pk(self.k_arr[j]*self.cosmology.pars['h'], z_ini) for j in xrange(self.len_k)])
            PK_10 *= self.cosmology.pars['h']**3

            # Integrand for growth factor with gamma
            E2_a = lambda a: (self.E_z(1./a-1.))**2.
            integrand = lambda a_int: (self.cosmology.Omega_m() / a_int**3 / E2_a(a_int))**self.gamma / a_int

            # Calculate D(z) and get P(k,z)
            for i in range(self.len_z):
                a_final = 1./(1.+self.z_arr[i])
                integral = quad(integrand,a_ini,a_final)[0]
                D_z = (np.exp(integral))**2
                Pk[:,i] = PK_10*D_z

        # Get sigma_8
        sigma8 = self.calc_sigma8(self.k_arr, Pk[:, 0])  # here with neutrinos

        return Pk, sigma8

    def calc_sigma8(self, k, P):
        window = 3.*(np.sin(8.*k)*(8.*k)**-3 - np.cos(8.*k)*(8.*k)**-2)

        return (np.trapz(P * window**2 * k**2, k)/2.)**.5/np.pi

    def calc_sigma2_r(self, r, k, P):
        kr = k*r
        window = 3.*(np.sin(kr)*kr**-3. - np.cos(kr)*kr**-2.)
        integrand = P * window**2. * k**2.

        return np.trapz(integrand, k)/2.*np.pi**-2.

    def calcMF(self, M_arr, MPK):

        rho_crit = 2.775362e11  # in h^2 Msun/Mpc^3
        rho_m = rho_crit * (self.cosmology.pars['Omega_b']+self.cosmology.pars['Omega_cdm'])

        Tinkersplines = self.Init_Tinker()

        massfunction = np.empty([self.len_z,self.len_sel_obs])

        r_array = (3.*M_arr/4./np.pi/rho_m)**(1./3.)

        Ez = self.E_z(self.z_arr)

        for i in range(self.len_z):

            sigma2 = np.array([self.calc_sigma2_r(r_array[i, j], self.k_arr, MPK[:, i])
                               for j in range(self.len_sel_obs)])

            # d(sigma^2)/dM
            dsigma2dM = np.array([.5*((sigma2[j+1]-sigma2[j]) / (M_arr[i,j+1]-M_arr[i,j]) + \
                                       (sigma2[j]-sigma2[j-1]) / (M_arr[i,j]-M_arr[i,j-1])) \
                                  for j in range(1,self.len_sel_obs-1)])
            dsigma2dM = np.insert(dsigma2dM, 0, .5*(sigma2[1]-sigma2[0])/(M_arr[i,1]-M_arr[i,0]))
            dsigma2dM = np.append(dsigma2dM,
                                  .5*(sigma2[self.len_sel_obs-1]-sigma2[self.len_sel_obs-2])/(M_arr[i, self.len_sel_obs-1]-M_arr[i, self.len_sel_obs-2]))

            # Delta_mean
            Omega_m_z = self.cosmology.Omega_m()*(1.+self.z_arr[i])**3/Ez[i]**2
            Deltamean = 500./Omega_m_z

            MF_A, MF_a, MF_b, MF_c = self.params_Tinker(self.z_arr[i], Deltamean, Tinkersplines)

            fsigma = MF_A * ((sigma2**.5/MF_b)**-MF_a + 1.) * np.exp(-MF_c/sigma2)

            # dN/dlog10M0
            massfunction[i, :] = - fsigma * rho_m * dsigma2dM/2./sigma2 * np.log(10.)

        return massfunction

    def Init_Tinker(self):
        x = np.log((200., 300., 400., 600., 800., 1200., 1600., 2400., 3200.))

        # A
        y = (1.858659e-01, 1.995973e-01, 2.115659e-01, 2.184113e-01, 2.480968e-01, 2.546053e-01, 2.600000e-01,
             2.600000e-01, 2.600000e-01)
        f1 = interp1d(x, y, kind='cubic')

        # a
        y = (1.466904e+00, 1.521782e+00, 1.559186e+00, 1.614585e+00, 1.869936e+00, 2.128056e+00, 2.301275e+00,
             2.529241e+00, 2.661983e+00)
        f2 = interp1d(x, y, kind='cubic')

        # b
        y = (2.571104e+00, 2.254217e+00, 2.048674e+00, 1.869559e+00, 1.588649e+00, 1.507134e+00, 1.464374e+00,
             1.436827e+00, 1.405210e+00)
        f3 = interp1d(x, y, kind='cubic')

        # c
        y = (1.193958e+00, 1.270316e+00, 1.335191e+00, 1.446266e+00, 1.581345e+00, 1.795050e+00, 1.965613e+00,
             2.237466e+00, 2.439729e+00)
        f4 = interp1d(x, y, kind='cubic')

        return [f1, f2, f3, f4]

    def params_Tinker(self, z, Deltamean, splines):
        logDeltamean = np.log(Deltamean)
        if Deltamean > 3200:
            z0params = [.26, 2.66, 1.41, 2.44]
        else:
            z0params = [splines[0](logDeltamean), splines[1](logDeltamean), splines[2](logDeltamean),
                        splines[3](logDeltamean)]
        logalpha = -(.75/np.log10(Deltamean/75.))**1.2
        alpha = 10.**logalpha

        A = z0params[0] * (1.+z)**-.14
        a = z0params[1] * (1.+z)**-.06
        b = z0params[2] * (1.+z)**-alpha
        c = z0params[3] * 1.

        return A, a, b, c


class XRayLuminosityLikelihoodModule(object):

    def __init__(self,
                 astro_mapping=CLUSTER_PARAMETER_MAPPING,
                 sel_obs_min=42, sel_obs_max=47, sel_obs_bin=120,
                 z_min=0., z_max=2., z_bin=200,
                 catalog_path='Mock_flux_lim.txt',
                 tol=1.e-6,
                 debug_mode=False):

        self.astro_mapping = astro_mapping

        self.len_sel_obs = sel_obs_bin #mass and selection observable bin have same size
        self.len_z = z_bin #number of redshift bins
        self.sel_obs_arr = np.logspace(sel_obs_min, sel_obs_max, sel_obs_bin) #sel.obs. array, will be converted to mass
        self.dlnL = np.log(self.sel_obs_arr[1])-np.log(self.sel_obs_arr[0])
        self.z_arr = np.linspace(z_min, z_max, z_bin) #redshift array

        self.catalog_path = catalog_path

        # tolerance in number count
        self.tol = tol

        # debug mode, writing more stuff in self for check in notebook
        self.debug = debug_mode

        return

    def setup(self):

        # read in catalog, in .txt format, 0th colunm redshift, 2nd column Luminosity
        # adapt this to your catalog if needed
        z, LX = np.genfromtxt(self.catalog_path)[[0, 2]]

        # number of cluster observed in redshift bin
        self.n = np.histogram2d(z, LX, bins=[self.z_arr, self.sel_obs_arr])[0]

        # fixed cosmology as in mock catalogue to apply flux cut
        params = {'h': 0.738, 'Omega_b': 0.0404, 'Omega_cdm': 0.25, 'n_s': 0.96, 'ln10^{10}A_s': 3}
        self.cosmology = Class()
        self.cosmology.set(params)
        self.cosmology.compute()

        self.f_cut = 4.4e-14
        Mpc2cm = 3.086e+24
        self.L_cut = 1.e43

        # luminosity distance in cm
        dlum = np.array([self.cosmology.luminosity_distance(self.z_arr[i])*Mpc2cm for i in xrange(self.len_z)])
        dlum[dlum == 0] = 1.e-15  # avoid division by zero
        # compute the flux grid
        self.fX = np.array([self.sel_obs_arr/(4*np.pi*dlum[i]**2) for i in xrange(self.len_z)])

        # luminosity edges as a grid
        self.LX = np.array([self.sel_obs_arr for i in xrange(self.len_z)])

        return

    def __call__(self, ctx):

        return self.computeLikelihood(ctx)

    def computeLikelihood(self, ctx):

        p1 = ctx.getParams()

        astro_params = {}
        for key, value in self.astro_mapping.items():
            astro_params[key] = p1[value]  # saving the relevant astrophysical parameters

        # get theory prediction from context
        dNdM = ctx.get('Massfunction')

        # transform the dNdM into a prediction for the luminosity
        dNdlnLX = self.transform2luminosity(dNdM, astro_params)

        # predict the number of cluster in each bin
        N = dNdlnLX*self.dlnL

        # mask accounting for selection criteria
        mask = self.selection_mask()
        if self.debug:
            self.mask = mask

        # mask bins outside of selection criterion
        N *= mask
        n_obs = self.n*mask
        if self.debug:
            self.n_obs = n_obs

        # prevent to take log of zero
        inds = np.where(N > 0)

        if self.debug:
            self.N = N

        # compute the likelihood assuming Poisson noise
        logL = np.sum(n_obs[inds]*np.log(N[inds])) - np.sum(N[inds])

        return logL

    def transform2luminosity(self, dNdM, astro_params):

        ALM = astro_params['A_LM']
        DLM = astro_params['D_LM']

        # remember that dNdM actually is dNdlog10M
        dNdlog10LX = dNdM/ALM

        # transform to dNdlnLX, where the intrinsic scatter is given by a gaussian
        dNdlnLX = dNdlog10LX/np.log(10)

        sigma = DLM/self.dlnL  # smoothing scale in pixels
        smoothed_dNdlnLX = gaussian_filter(dNdlnLX, sigma=(0., sigma))

        return smoothed_dNdlnLX

    def selection_mask(self):

        mask = np.ones((self.len_z, self.len_sel_obs))

        # accounting for flux cut
        mask = np.where(self.fX < self.f_cut, 0, mask)

        # accounting for luminosity cut
        mask = np.where(self.LX < self.L_cut, 0, mask)

        # only consider bins, where all edges are unmasked
        tmp = 0.25*(mask[1:, :-1] + mask[1:, 1:] + mask[:-1, :-1] + mask[:-1, 1:])
        mask = tmp.astype(int)

        return mask


class BigBangNucleoSynthesisPrior(object):

    def __init__(self, mean=0.02202, sigma=0.00045, cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING):
        #default is the value reported by Cooke et al. 2014

        self.mean = mean
        self.sigma = sigma
        self.mapping = cosmo_mapping

        return

    def setup(self):

        return

    def __call__(self, ctx):

        return self.computeLikelihood(ctx)

    def computeLikelihood(self, ctx):

        p1 = ctx.getParams()

        try:
            index = self.mapping['omega_b']
            value = p1[index]
        except KeyError:
            tmp = 0

        try:
            index1 = self.mapping['Omega_b']
            index2 = self.mapping['h']
            value = p1[index1]*p1[index2]**2
        except KeyError:
            tmp = 0

        return -0.5*(value - self.mean)**2/self.sigma**2


class HubbleConstantPrior(object):

    def __init__(self, mean=0.7388, sigma=0.024, cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING):
        # following the measurement of Riess et al. 2011

        self.mean = mean
        self.sigma = sigma
        self.mapping = cosmo_mapping

        return

    def setup(self):

        return

    def __call__(self, ctx):

        return self.computeLikelihood(ctx)

    def computeLikelihood(self, ctx):

        p1 = ctx.getParams()

        index = self.mapping['h']
        value = p1[index]

        result = - 0.5*(value-self.mean)**2/self.sigma**2

        return result


VIKHLININ_MEANS = {'A_LM': 1.61,
                   'B_LM': 101.483,
                   'C_LM': 1.85,
                   'D_LM': 0.396}

VIKHLININ_ERRORS = {'A_LM': 0.14,
                    'B_LM': 0.085,
                    'C_LM': 0.42,
                    'D_LM': 0.039}


class MassLuminisityPrior(object):

    def __init__(self, means=VIKHLININ_MEANS, sigmas=VIKHLININ_ERRORS, mapping=CLUSTER_PARAMETER_MAPPING):

        self.means = means
        self.sigmas = sigmas
        self.mapping = mapping

        return

    def setup(self):

        return

    def __call__(self, ctx):

        return self.computeLikelihood(ctx)

    def computeLikelihood(self, ctx):

        p1 = ctx.getParams()

        result = 0
        for key, index in self.mapping.items():
            result += (p1[index]-self.means[key])**2/self.sigmas[key]**2

        return -0.5*result


class CosmologyModuleClass(object):

    def __init__(self, constants=DEFAULT_PARAMS,
                 output=CLASS_OUTPUT_PARAMS,
                 cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING):

        self.cosmo_mapping = cosmo_mapping
        self.constants = constants
        self.output = output
        self.state = False

        return

    def setup(self):

        self.cosmology = Class()

        return

    def __call__(self, ctx):

        if self.state:
            self.cosmology.struct_cleanup()

        p1 = ctx.getParams()

        cosmo_params = self.constants.copy()  # saving the constants
        cosmo_params.update(self.output)  # appending output parameters

        for key, value in self.cosmo_mapping.items():
            cosmo_params[key] = p1[value]  # adding the relevant cosmological parameters

        # popping gamma and setting for modified growth
        try:
            self.gamma = cosmo_params.pop('gamma')
            if self.gamma != 0.55:
                cosmo_params['z_max_pk'] = 10.
        except KeyError:
            tmp = 0

        # set up correctly the dark energy density
        de_perturb = cosmo_params.pop('DE_perturb')
        try:
            if cosmo_params['w0_fld'] == -1. and cosmo_params['wa_fld']==0:
                cosmo_params.pop('w0_fld')
                cosmo_params.pop('wa_fld')
            else:
                if de_perturb:  # include neutrino density here
                    cosmo_params['Omega_fld'] = 1.-cosmo_params.pop('Omega_k')-cosmo_params['Omega_b']-cosmo_params['Omega_cdm']
                else:
                    cosmo_params['Omega_Lambda'] = 1.-cosmo_params.pop('Omega_k')-cosmo_params['Omega_b']-cosmo_params['Omega_cdm']
        except KeyError:
            tmp = 0

        self.cosmology.set(cosmo_params)
        self.cosmology.compute()

        ctx.add('cosmology', self.cosmology)

        self.state=True

        return


def converter_Class2Camb(cosmo_params):
    output = {}
    try:
        output['H0'] = 100*cosmo_params.pop('h')
    except KeyError: pass
    try:
        output['omegab'] = cosmo_params.pop('Omega_b')
    except KeyError: pass
    try:
        output['omegac'] = cosmo_params.pop('Omega_cdm')
    except KeyError: pass
    try:
        output['scalar_index'] = cosmo_params.pop('n_s')
    except KeyError: pass
    try:
        output['scalar_amp'] = 1.e-10*np.exp(cosmo_params.pop('ln10^{10}A_s'))
    except KeyError: pass
    try:
        output['omegak'] = cosmo_params.pop('Omega_k')
    except KeyError: pass
    try:
        output['w_perturb'] = cosmo_params.pop('DE_perturb')
    except KeyError: pass
    try:
        output['w_lam'] = cosmo_params.pop('w0_fld')
    except KeyError: pass
    try:
        output['omegav'] = 1. - output['omegab'] - output['omegac'] - output['omegak']
    except KeyError: pass

    return output


class MassfunctionCoreModule_PyCamb(object):

    def __init__(self, constants=DEFAULT_PARAMS,
                 output=CLASS_OUTPUT_PARAMS,
                 cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING,
                 astro_mapping=CLUSTER_PARAMETER_MAPPING,
                 selection_observable='LX',
                 sel_obs_min=42, sel_obs_max=47, sel_obs_bin=120,
                 z_min=0., z_max=2., z_bin=200,
                 k_min=-5., k_max=1., k_bin=500,
                 f_sky=0.68):

        self.cosmo_mapping = cosmo_mapping
        self.astro_mapping = astro_mapping
        self.constants = constants
        self.output = output

        # flag determining the selection variable, only implemented LX selection
        self.selection_observable = selection_observable

        # setting up the grid in selection observable and redshift
        self.len_sel_obs = sel_obs_bin  # mass and selection observable bin have same size
        self.len_z = z_bin  # number of redshift bins
        self.len_k = k_bin
        # sel.obs. array, will be converted to mass
        self.sel_obs_arr = np.logspace(sel_obs_min, sel_obs_max, sel_obs_bin)
        self.z_arr = np.linspace(z_min, z_max, z_bin)  # redshift array
        self.k_arr = np.logspace(k_min, k_max, k_bin)  # wavenumber array

        # modified gravity needs special treatment
        self.gamma = 0.55

        # fraction of skz covered by the survey
        self.f_sky = f_sky

        return

    def setup(self):

        return

    def __call__(self, ctx):

        p1 = ctx.getParams()

        cosmo_params = self.constants.copy()  # saving the constants
        cosmo_params.update(self.output)  # appending output parameters

        for key, value in self.cosmo_mapping.items():
            cosmo_params[key] = p1[value]  # adding the relevant cosmological parameters

        # popping gamma and setting for modified growth
        try:
            self.gamma = cosmo_params.pop('gamma')
        except KeyError:
            pass

        # convert to Camb notation
        cosmo_params = converter_Class2Camb(cosmo_params)

        astro_params = {}
        for key, value in self.astro_mapping.items():
            astro_params[key] = p1[value]  # saving the relevant astrophysical parameters

        # getting the mass array (in units of h M_solar) depending on cosmology and astrophysics
        M_arr = self.get_mass_array(astro_params, cosmo_params)

        # computing the Matter Powerspectrum
        mPk, sigma8 = self.get_MatterPowerspectrum_Camb(cosmo_params)

        # adding current sigma8 and Omega_m to the data, can be logged using ExtSampleFileUtil.py as storageFile
        ctx.getData()['sigma8'] = sigma8
        ctx.getData()['Omega_m'] = cosmo_params['omegac']+cosmo_params['omegab']

        # compute the number density of clusters on the grid defined by self.z_arr and M_arr
        # following Tinker et al. 2008
        dndM = self.calcMF(M_arr, mPk, cosmo_params)

        # compute the redshift volume
        dV_z = self.zVol(cosmo_params)

        # getting the massfunction
        dNdM = dndM*dV_z

        # normalizing for the covered sky fraction
        dNdM *= 4*np.pi*self.f_sky

        # average the mass function to the center of the bin
        dNdM_average = 0.25*(dNdM[1:, :-1]+dNdM[1:, 1:]+dNdM[:-1, :-1]+dNdM[:-1, 1:])

        ctx.add('Massfunction', dNdM_average)
        ctx.add('M_arr', M_arr)

        return

    def E_z(self, z, cosmo_params):

        Om = cosmo_params['omegac']+cosmo_params['omegab']
        Ok = cosmo_params['omegak']
        Ol = cosmo_params['omegav']
        Or = 5.2e-5
        w = cosmo_params['w_lam']
        friedman = Om*(1+z)**3 + Or*(1+z)**4 + Ok*(1+z)**2 + Ol*(1+z)**(3*(1+w))

        return np.sqrt(friedman)

    def zVol(self, cosmo_params):

        dz = self.z_arr[2] - self.z_arr[1]
        c = 2997.92458  # speed of light in 100 km/s, Hubble distance in Mpc/h

        # Angular diameter distance in units of Mpc/h
        AngDiamDist_arr = pc.angular_diameter(self.z_arr[::-1], **cosmo_params)[::-1]
        AngDiamDist_arr *= cosmo_params['H0']/100.

        # comoving Volume per redshift bin
        deltaV = c*((1.+self.z_arr)*AngDiamDist_arr)**2 / self.E_z(self.z_arr, cosmo_params)

        # ensuring that dV(z=0) != 0
        deltaV[0] = deltaV[1]

        return dz*deltaV[:, None]

    def get_mass_array(self, astro_params, cosmo_params):
        # sets up the mass grid in units of M_solar/h following the respective scaling relations

        if self.selection_observable == 'LX':

            lnLX = np.log(self.sel_obs_arr)
            ALM = astro_params['A_LM']
            BLM = astro_params['B_LM']
            CLM = astro_params['C_LM']
            DLM = astro_params['D_LM']

            h = cosmo_params['H0']/100
            Ez = self.E_z(self.z_arr, cosmo_params)

            # apply the scaling relation from Vikhlinin et al. 2009
            lnM = np.array([1./ALM*lnLX - CLM/ALM*np.log(Ez[i]) for i in xrange(self.len_z)])
            lnM -= BLM/ALM + 1.5/ALM*(DLM**2-0.396**2)
            lnM += 0.39/ALM*np.log(h/0.72)+np.log(3.9*10**14)

            # converting to masses in M_solar/h
            return np.exp(lnM)*h

    def get_MatterPowerspectrum_Camb(self, cosmo_params):

        Om = cosmo_params['omegac']+cosmo_params['omegab']

        # for standard growth
        if self.gamma == .55:
            # Camb need k in h/Mpc and gives out the powerspectrum in units of h3/Mpc3
            out, Pk_out = pc.matter_power(self.z_arr[::-1], self.k_arr, **cosmo_params)
            Pk = Pk_out[:, ::-1]

        # for modified growth
        else:
            Pk = np.empty((self.len_k, self.len_z))
            # Get PS at redshift z_ini
            z_ini = 10.
            a_ini = 1./(1.+z_ini)
            PK_10 = pc.matter_power([z_ini], self.k_arr, **cosmo_params)

            # Integrand for growth factor with gamma
            E2_a = lambda a: (self.E_z(1./a-1., cosmo_params))**2.
            integrand = lambda a_int: (Om / a_int**3 / E2_a(a_int))**self.gamma / a_int

            # Calculate D(z) and get P(k,z)
            for i in range(self.len_z):
                a_final = 1./(1.+self.z_arr[i])
                integral = quad(integrand,a_ini,a_final)[0]
                D_z = (np.exp(integral))**2
                Pk[:, i] = PK_10*D_z

        sigma8 = self.calc_sigma8(self.k_arr, Pk[:, 0])

        return Pk, sigma8

    def calc_sigma8(self, k, P):
        window = 3.*(np.sin(8.*k)*(8.*k)**-3 - np.cos(8.*k)*(8.*k)**-2)
        integrand = P * window**2 * k**2

        return (trapz(integrand, k)/2.)**.5/np.pi

    def calc_sigma2_r(self, r, k, P):
        kr = k*r
        window = 3.*(np.sin(kr)*kr**-3. - np.cos(kr)*kr**-2.)
        integrand = P * window**2. * k**2.

        return trapz(integrand, k)/2.*np.pi**-2.

    def calcMF(self, M_arr, MPK, cosmo_params):

        Om = cosmo_params['omegac']+cosmo_params['omegab']
        rho_crit = 2.775362e11 # in h^2 Msun/Mpc^3
        rho_m = rho_crit * Om

        Tinkersplines = self.Init_Tinker()

        massfunction = np.empty([self.len_z, self.len_sel_obs])

        r_array = (3.*M_arr/4./np.pi/rho_m)**(1./3.)

        Ez = self.E_z(self.z_arr, cosmo_params)

        for i in range(self.len_z):

            sigma2 = np.array([self.calc_sigma2_r(r_array[i, j], self.k_arr, MPK[:, i])
                               for j in range(self.len_sel_obs)])

            # d(sigma^2)/dM
            dsigma2dM = np.array([.5*((sigma2[j+1]-sigma2[j]) / (M_arr[i,j+1]-M_arr[i,j]) + \
                                       (sigma2[j]-sigma2[j-1]) / (M_arr[i,j]-M_arr[i,j-1])) \
                                  for j in range(1,self.len_sel_obs-1)])
            dsigma2dM = np.insert(dsigma2dM, 0, .5*(sigma2[1]-sigma2[0])/(M_arr[i,1]-M_arr[i,0]))
            dsigma2dM = np.append(dsigma2dM,
                                  .5*(sigma2[self.len_sel_obs-1]-sigma2[self.len_sel_obs-2])/(M_arr[i, self.len_sel_obs-1]-M_arr[i, self.len_sel_obs-2]))

            # Delta_mean
            Omega_m_z = Om*(1.+self.z_arr[i])**3/Ez[i]**2
            Deltamean = 500./Omega_m_z

            MF_A, MF_a, MF_b, MF_c = self.params_Tinker(self.z_arr[i], Deltamean, Tinkersplines)

            fsigma = MF_A * ((sigma2**.5/MF_b)**-MF_a + 1.) * np.exp(-MF_c/sigma2)

            # dN/dlog10M0
            massfunction[i, :] = - fsigma * rho_m * dsigma2dM/2./sigma2 * np.log(10.)

        return massfunction

    def Init_Tinker(self):
        x = np.log((200., 300., 400., 600., 800., 1200., 1600., 2400., 3200.))

        # A
        y = (1.858659e-01, 1.995973e-01, 2.115659e-01, 2.184113e-01, 2.480968e-01, 2.546053e-01, 2.600000e-01,
             2.600000e-01, 2.600000e-01)
        f1 = interp1d(x, y, kind='cubic')

        # a
        y = (1.466904e+00, 1.521782e+00, 1.559186e+00, 1.614585e+00, 1.869936e+00, 2.128056e+00, 2.301275e+00,
             2.529241e+00, 2.661983e+00)
        f2 = interp1d(x, y, kind='cubic')

        # b
        y = (2.571104e+00, 2.254217e+00, 2.048674e+00, 1.869559e+00, 1.588649e+00, 1.507134e+00, 1.464374e+00,
             1.436827e+00, 1.405210e+00)
        f3 = interp1d(x, y, kind='cubic')

        # c
        y = (1.193958e+00, 1.270316e+00, 1.335191e+00, 1.446266e+00, 1.581345e+00, 1.795050e+00, 1.965613e+00,
             2.237466e+00, 2.439729e+00)
        f4 = interp1d(x, y, kind='cubic')

        return [f1, f2, f3, f4]

    def params_Tinker(self, z, Deltamean, splines):
        logDeltamean = np.log(Deltamean)
        if Deltamean > 3200:
            z0params = [.26, 2.66, 1.41, 2.44]
        else:
            z0params = [splines[0](logDeltamean), splines[1](logDeltamean), splines[2](logDeltamean),
                        splines[3](logDeltamean)]
        logalpha = -(.75/np.log10(Deltamean/75.))**1.2
        alpha = 10.**logalpha

        A = z0params[0] * (1.+z)**-.14
        a = z0params[1] * (1.+z)**-.06
        b = z0params[2] * (1.+z)**-alpha
        c = z0params[3] * 1.

        return A, a, b, c


class MassfunctionCoreModule_v2_PyCamb(object):

    def __init__(self, constants=DEFAULT_PARAMS,
                 output=CLASS_OUTPUT_PARAMS,
                 cosmo_mapping=COSMOLOGY_PARAMETER_MAPPING,
                 astro_mapping=CLUSTER_PARAMETER_MAPPING,
                 selection_observable='LX',
                 sel_obs_min=42, sel_obs_max=47, sel_obs_bin=120,
                 z_min=0., z_max=2., z_bin=200,
                 k_min=-5., k_max=1., k_bin=500,
                 f_sky=0.68):

        self.cosmo_mapping = cosmo_mapping
        self.astro_mapping = astro_mapping
        self.constants = constants
        self.output = output

        # flag determining the selection variable, only implemented LX selection
        self.selection_observable = selection_observable

        # setting up the grid in selection observable and redshift
        self.len_sel_obs = sel_obs_bin  # mass and selection observable bin have same size
        self.len_z = z_bin  # number of redshift bins
        self.len_k = k_bin
        # sel.obs. array, will be converted to mass
        self.sel_obs_arr = np.logspace(sel_obs_min, sel_obs_max, sel_obs_bin)
        self.z_arr = np.linspace(z_min, z_max, z_bin)  # redshift array
        self.k_arr = np.logspace(k_min, k_max, k_bin)  # wavenumber array

        # modified gravity needs special treatment
        self.gamma = 0.55

        # fraction of skz covered by the survey
        self.f_sky = f_sky

        return

    def setup(self):

        # setup interpolation for mass function (MF)
        self.splines_MF = self.Init_Tinker_MF()
        z0params_MF = np.array([.26, 2.66, 1.41, 2.44])
        self.z0params_MF = np.outer(z0params_MF, np.ones(self.len_z))

        return

    def Init_Tinker_MF(self):
        """
        :return: initializes the spline for interpolation the mass function by Tinker et al. 2008
        """
        x = np.log((200., 300., 400., 600., 800., 1200., 1600., 2400., 3200.))

        # A
        y = (1.858659e-01, 1.995973e-01, 2.115659e-01, 2.184113e-01, 2.480968e-01, 2.546053e-01, 2.600000e-01,
             2.600000e-01, 2.600000e-01)
        f1 = interp1d(x, y, kind='cubic')

        # a
        y = (1.466904e+00, 1.521782e+00, 1.559186e+00, 1.614585e+00, 1.869936e+00, 2.128056e+00, 2.301275e+00,
             2.529241e+00, 2.661983e+00)
        f2 = interp1d(x, y, kind='cubic')

        # b
        y = (2.571104e+00, 2.254217e+00, 2.048674e+00, 1.869559e+00, 1.588649e+00, 1.507134e+00, 1.464374e+00,
             1.436827e+00, 1.405210e+00)
        f3 = interp1d(x, y, kind='cubic')

        # c
        y = (1.193958e+00, 1.270316e+00, 1.335191e+00, 1.446266e+00, 1.581345e+00, 1.795050e+00, 1.965613e+00,
             2.237466e+00, 2.439729e+00)
        f4 = interp1d(x, y, kind='cubic')

        return [f1, f2, f3, f4]

    def __call__(self, ctx):

        p1 = ctx.getParams()

        cosmo_params = self.constants.copy()  # saving the constants
        cosmo_params.update(self.output)  # appending output parameters

        for key, value in self.cosmo_mapping.items():
            cosmo_params[key] = p1[value]  # adding the relevant cosmological parameters

        # popping gamma and setting for modified growth
        try:
            self.gamma = cosmo_params.pop('gamma')
        except KeyError:
            pass

        # convert to Camb notation
        cosmo_params = converter_Class2Camb(cosmo_params)

        astro_params = {}
        for key, value in self.astro_mapping.items():
            astro_params[key] = p1[value]  # saving the relevant astrophysical parameters

        # getting the mass array (in units of h M_solar) depending on cosmology and astrophysics
        M_arr = self.get_mass_array(astro_params, cosmo_params)

        # computing the Matter Powerspectrum
        mPk, sigma8 = self.get_MatterPowerspectrum_Camb(cosmo_params)

        # adding current sigma8 and Omega_m to the data, can be logged using ExtSampleFileUtil.py as storageFile
        ctx.getData()['sigma8'] = sigma8
        ctx.getData()['Omega_m'] = cosmo_params['omegac']+cosmo_params['omegab']

        # compute the sigma2(M,z) for M_arr given the matter power spectrum mPK
        sigma2 = self.get_sigma2(M_arr, mPk, cosmo_params)

        # compute the number density of clusters on the grid defined by self.z_arr and M_arr
        # following Tinker et al. 2008
        dndM = self.calcMF(M_arr, sigma2, cosmo_params)

        # compute the redshift volume
        dV_z = self.zVol(cosmo_params)

        # getting the massfunction
        dNdM = dndM*dV_z[:, None]

        # normalizing for the covered sky fraction
        dNdM *= 4*np.pi*self.f_sky

        ctx.add('Massfunction', dNdM)
        ctx.add('M_arr', M_arr)
        ctx.add('sigma2', sigma2)

        return

    def E_z(self, z, cosmo_params):

        Om = cosmo_params['omegac']+cosmo_params['omegab']
        Ok = cosmo_params['omegak']
        Ol = cosmo_params['omegav']
        Or = 5.2e-5
        w = cosmo_params['w_lam']
        friedman = Om*(1+z)**3 + Or*(1+z)**4 + Ok*(1+z)**2 + Ol*(1+z)**(3*(1+w))

        return np.sqrt(friedman)

    def zVol(self, cosmo_params):
        """
        :param cosmo_params: dictionary containing cosmological parameters
        :return: comoving volume per solid angle contained in the redshift bins, shape (len_z-1)
        """

        c = 2997.92458  # speed of light in 100 km/s, Hubble distance in Mpc/h

        # Angular diameter distance in units of Mpc/h
        AngDiamDist_arr = pc.angular_diameter(self.z_arr[::-1], **cosmo_params)[::-1]
        AngDiamDist_arr *= cosmo_params['H0']/100.

        # comoving Volume per redshift bin
        deltaV = c*((1.+self.z_arr)*AngDiamDist_arr)**2 / self.E_z(self.z_arr, cosmo_params)

        return 0.5*(self.z_arr[1:]-self.z_arr[:-1])*(deltaV[1:]+deltaV[:-1])

    def get_mass_array(self, astro_params, cosmo_params):
        """
        :param astro_params: dictionary containing the scaling relation parameters
        :param cosmo_params: dictionary of cosmological parameters to be handed to CAMB
        :return: sets up the mass grid in units of M_solar/h following the respective scaling relations
        """

        if self.selection_observable == 'LX':

            lnLX = np.log(self.sel_obs_arr)
            ALM = astro_params['A_LM']
            BLM = astro_params['B_LM']
            CLM = astro_params['C_LM']
            DLM = astro_params['D_LM']

            h = cosmo_params['H0']/100
            Ez = self.E_z(self.z_arr, cosmo_params)

            # apply the scaling relation from Vikhlinin et al. 2009
            lnM = 1./ALM*lnLX[None, :] - CLM/ALM*np.log(Ez[:, None])
            lnM -= BLM/ALM + 1.5/ALM*(DLM**2-0.396**2)
            lnM += 0.39/ALM*np.log(h/0.72)+np.log(3.9*10**14)

            # converting to masses in M_solar/h
            return np.exp(lnM)*h

    def get_MatterPowerspectrum_Camb(self, cosmo_params):
        """
        :param cosmo_params: dictionary of cosmological parameters to be handed to CAMB
        :return: matter power spectrum, shape (len_k, len_z), computed also for the case of modified growth
        """

        Om = cosmo_params['omegac']+cosmo_params['omegab']

        # for standard growth
        if self.gamma == .55:
            # Camb need k in h/Mpc and gives out the powerspectrum in units of h3/Mpc3
            _, Pk_out = pc.matter_power(self.z_arr[::-1], self.k_arr, **cosmo_params)
            Pk = Pk_out[:, ::-1]  # CAMB output is in cronological order, need to turn it around

        # for modified growth
        else:
            Pk = np.empty((self.len_k, self.len_z))
            # Get PS at redshift z_ini
            z_ini = 10.
            a_ini = 1./(1.+z_ini)
            PK_10 = pc.matter_power([z_ini], self.k_arr, **cosmo_params)

            # Integrand for growth factor with gamma
            E2_a = lambda a: (self.E_z(1./a-1., cosmo_params))**2.
            integrand = lambda a_int: (Om / a_int**3 / E2_a(a_int))**self.gamma / a_int

            # Calculate D(z) and get P(k,z)
            for i in range(self.len_z):
                a_final = 1./(1.+self.z_arr[i])
                integral = quad(integrand,a_ini,a_final)[0]
                D_z = (np.exp(integral))**2
                Pk[:, i] = PK_10*D_z

        # sigma8 at redshift 0
        sigma8 = self.calc_sigma8(Pk[:, 0])

        return Pk, sigma8

    def calc_sigma8(self, P):
        """
        :param P: matter power spectrum
        :return: computes sigma8
        """
        window = 3.*(np.sin(8.*self.k_arr)*(8.*self.k_arr)**-3 - np.cos(8.*self.k_arr)*(8.*self.k_arr)**-2)
        integrand = P * window**2 * self.k_arr**2

        return (trapz(integrand, self.k_arr)/2.)**.5/np.pi

    def get_sigma2(self, M_arr, Pk, cosmo_params):
        """
        :param M_arr: array of masses, shape (len_z, len_sel_obs)
        :param Pk: matter power spectrum, shape (len_k, len_z)
        :return: compute the rms of fluctuations sigma2(M,z), shape (len_z, len_sel_obs), by integrating the matter
        power spectrum with an appropriate top hat window function W_M(k)
        """

        Om = cosmo_params['omegac']+cosmo_params['omegab']
        rho_crit = 2.775362e11 # in h^2 Msun/Mpc^3
        rho_m = rho_crit * Om

        # compute radia associated the the masses M_arr
        r_arr = (3.*M_arr/4./np.pi/rho_m)**(1./3.)

        # Fourier transform of tophat window function of radius r
        kr = self.k_arr[:, None, None]*r_arr[None, :, :]
        window = 3.*(np.sin(kr)*kr**-3. - np.cos(kr)*kr**-2.)  # shape (len_k, len_z, len_sel_obs)

        # sigma2(M) = int d^3k/(2pi)^3 W_M(k)^2 P(k)
        integrand = 1./(2.*np.pi**2)*Pk[:, :, None]*window**2*self.k_arr[:, None, None]**2
        sigma2 = trapz(integrand, self.k_arr[:, None, None])

        return sigma2

    def calcMF(self, M_arr, sigma2, cosmo_params):
        """
        :param M_arr: Mass array, shape (len_z, len_sel_obs)
        :param sigma2: sigma2 array, shape (len_z, len_sel_obs)
        :param cosmo_params: dictionary of cosmological parameters
        :return: return the mass function dn/dlog10M0 at the center of each bin, shape (len_z-1, len_sel_obs-1)
        """

        Om = cosmo_params['omegac']+cosmo_params['omegab']
        rho_crit = 2.775362e11 # in h^2 Msun/Mpc^3
        rho_m = rho_crit * Om

        dsigma2dM = (sigma2[:, 1:] - sigma2[:, :-1])/(M_arr[:, 1:] - M_arr[:, :-1])

        # Delta_mean
        Omega_m_z = Om*(1.+self.z_arr)**3/self.E_z(self.z_arr, cosmo_params)**2
        Deltamean = 500./Omega_m_z

        MF_A, MF_a, MF_b, MF_c = self.params_Tinker(self.z_arr, Deltamean, self.splines_MF)

        fsigma = MF_A[:, None] * ((sigma2**.5/MF_b[:, None])**-MF_a[:, None] + 1.) * np.exp(-MF_c[:, None]/sigma2)

        # average to center of bin
        dsigma2dM_average = 0.5*(dsigma2dM[1:]+dsigma2dM[:-1])
        sigma2_average = 0.25*(sigma2[1:, :-1]+sigma2[1:, 1:]+sigma2[:-1, :-1]+sigma2[:-1, 1:])
        fsigma_average = 0.25*(fsigma[1:, :-1]+fsigma[1:, 1:]+fsigma[:-1, :-1]+fsigma[:-1, 1:])

        # mass function dn/dlog10M0 at the center of each bin. Note that its shape is (len_z-1, len_sel_obs-1)
        massfunction = - fsigma_average * rho_m * dsigma2dM_average/2./sigma2_average * np.log(10.)

        return massfunction

    def params_Tinker(self, z, Deltamean, splines):
        """
        :param z: redshift
        :param Deltamean: ??
        :param splines: interpolation splines for Tinker mass function
        :return: parameters of Tinker mass function
        """
        logDeltamean = np.log(Deltamean)
        inds = np.where(Deltamean < 3200)[0]  # where Deltamean < 3200, apply the splines
        z0params = self.z0params_MF.copy()

        z0params[0, inds] = splines[0](logDeltamean[inds])
        z0params[1, inds] = splines[1](logDeltamean[inds])
        z0params[2, inds] = splines[2](logDeltamean[inds])
        z0params[3, inds] = splines[3](logDeltamean[inds])

        logalpha = -(.75/np.log10(Deltamean/75.))**1.2
        alpha = 10.**logalpha

        A = z0params[0] * (1.+z)**-.14
        a = z0params[1] * (1.+z)**-.06
        b = z0params[2] * (1.+z)**-alpha
        c = z0params[3] * 1.

        return A, a, b, c
